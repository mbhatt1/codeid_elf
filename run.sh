#!/bin/sh
start=`date +%s`
objcopy -O binary --only-section=.text $1 $2
echo extracted text section to "$2"
readelf -r  $1 > $3
echo relocs done.
python rel_entry.py $3 $4
echo offsets extracted.
rm -rf ./tmp/*
python split_offset_files.py $4 $2 > output1.txt
end1=`date +%s`
runtime1=$(python -c "print 'Detection Time: %u:%02u' % ((${end1} - ${start})/60, (${end1} - ${start})%60)")
echo "$runtime1"
echo done. with detection

python sort_pages.py output1.txt
echo done.
end=`date +%s`
runtime=$(python -c "print '%u:%02u' % ((${end} - ${start})/60, (${end} - ${start})%60)")
echo "$runtime"
