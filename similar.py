import os, sys, struct
from sets import Set
import binascii

# offsetfile = open(sys.argv[1], 'r')
# print "First, Compare to, per similar"
done_already = []
for line in os.listdir("./kernel"):
    to_compare_dictionary = set()
    os.system("readelf -r " + './kernel/'+ line + ' >' + 'rel_text1')
    os.system("python rel_entry.py rel_text1 offsets1.txt")
    offsets = [ (int(a.strip('\n'),16) - 0xc1000000) for a in open('offsets1.txt')]
    os.system("objcopy -O binary --only-section=.text ./kernel/" + line + " to_compare.text")
    f = open('./sign/' + line, 'w+')
    with open("to_compare.text", "rb") as to_compare:
        for offset in offsets:
            to_compare.seek(offset)
            add_to_set = str(hex(offset)) + str(binascii.hexlify(to_compare.read(4)))
            to_compare_dictionary.add(add_to_set)
            f.write (add_to_set)
            f.write('\n')
    for line1 in os.listdir("./kernel"):
        done_already_string = line1 + line
        done_already_string_rev = line + line1
        f1 = open('./sign/' + line1, 'w+')
        if done_already_string not in done_already and done_already_string_rev not in done_already:
            done_already.append(done_already_string_rev)
            done_already.append(done_already_string)
            compare_with_set = set()
            os.system("readelf -r " + './kernel/'+ line1 + ' >' + 'rel_text2')
            os.system("python rel_entry.py rel_text2 offsets2.txt")
            offsets2 = [ (int(b.strip('\n'),16) - 0xc1000000) for b in open('offsets2.txt')]
            os.system("objcopy -O binary --only-section=.text ./kernel/" + line1 + " compare_with.text")
            with open("compare_with.text", "rb") as compare_with:
                for offset in offsets2:
                    compare_with.seek(offset)
                    add_to_set = str(hex(offset)) + str(binascii.hexlify(compare_with.read(4)))
                    compare_with_set.add(add_to_set)
                    f1.write(add_to_set)
                    f1.write('\n')
            similarity = to_compare_dictionary & compare_with_set #INTERSECTION
            number_of_similar = len(similarity)
            per_similarity = number_of_similar/float(len(to_compare_dictionary))*100
            percent = float("{0:.4f}".format(per_similarity))
            if number_of_similar is not 0 and percent > .01 and percent < 100.0:
                print str(line[8:]) + ' & ' + str(line1[8:]) + ' & ' + str(number_of_similar) + ' & ' + str(percent) + '\\' + '\\'
        else:
            continue
