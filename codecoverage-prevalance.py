#!/usr/bin/python
import sys
import os
# from collections

infile = sys.argv[1]
vmlinuxfile = sys.argv[2]
# outfile = sys.argv[2]
# f = open(infile)
#Open the input file Above

lines = [ (int(line.rstrip('\n'),16)-0xc1000000) for line in open(infile)]



d = dict()
for i in lines:
    j = i & 0xfffff000
    d.setdefault(j, []).append(i)

no_signatures = len(lines)
# text = open('f.text', 'r')
total_pages = os.stat('./file.text').st_size/0x1000
page_addresses = [i for i in range(0x0, total_pages*0x1000, 0x1000)]
prevelance = no_signatures/float(total_pages)

#     print hex(key)
coverage = 0
matched = 0
for address in page_addresses:
    #out_filename = "./tmp/" + str(hex(key)) + '.text'
    # print len(d[key])
    if  (address in d.keys()):
        matched = matched + 1
    else:
        coverage = coverage + 1

code_coverage = matched / float(matched + coverage)
print os.path.basename(sys.argv[2]) + ' ,' + str(code_coverage) + ' ,' + str(total_pages)+' ,' + str(matched) + ' ,' + str(coverage)
