#!/bin/sh

for filename in ./kernel/*; do
    readelf -r $filename > rel.txt
    python rel_entry.py rel.txt off.txt
    objcopy -O binary --only-section=.text $filename file.text
    #echo $filename
    python codecoverage-prevalance.py off.txt $filename
done
