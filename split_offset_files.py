#!/usr/bin/python
import sys
import os
# from collections

infile = sys.argv[1]
# outfile = sys.argv[2]
# f = open(infile)
#Open the input file Above

lines = [ (int(line.rstrip('\n'),16)-0xc1000000) for line in open(infile)]

page_addresses = [i for i in range(0x0, 0xfff000, 0x1000) ]
d = dict()
for i in lines:
    j = i & 0xfffff000
    d.setdefault(j, []).append(i)

for key in d:
    out_filename = "./tmp/" + str(hex(key)) + '.text'

    if  (len(d[key]) == 0):
        print "list_empty"
    else:
        outfile = open(out_filename, 'w')
        for a in (d[key]):
            outfile.write((str(hex(a+ 0xc1000000)) + ' ' + '\n')[2:])
#Prints list of the pages detected
for filename in os.listdir('./tmp/'):
    os.system('./codeid '+ './tmp/'+ filename + ' '+ sys.argv[2] + ' ' + 'vmss.core' )
