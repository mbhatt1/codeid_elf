import sys

infile = sys.argv[1]

lines = [ (int(line.strip('\n'),16)) for line in open(infile)]
#lines.sort()
lines1 = set(lines)
lines = sorted(lines1)
d = dict()
for i in lines:
    j = i & 0xffff0000
    if j in d:
        d[j] = d[j] + 1
    else:
        d[j] =  1

    # with open('detected_bases.txt', "a+") as f:
    #     f.write( "Base Address " + str(hex(j & 0xff000000)) + '\n')

No_pages_matched = 0
No_pages_mismatched = 0
not_in_range = 0
total_pages_detected = 0
#How many pages aren't detected?
others = 0
for k,v in d.iteritems():
    total_pages_detected = total_pages_detected + v

    if v >14:
        No_pages_matched = No_pages_matched + v
    else:
        not_in_range = not_in_range + v

    if v!=16 and v>14:
        No_pages_mismatched = No_pages_mismatched + (16-v)

for k,v in d.iteritems():
    if v!=16 and v>14:
        print str(hex(k)) + ' <--Page Range Detected --> ' + str(hex(v))

others = total_pages_detected - ((No_pages_matched + No_pages_mismatched + not_in_range))
accuracy = (No_pages_matched + others)/float(No_pages_matched + No_pages_mismatched + not_in_range + others)

print 'total_pages_detected ' + str(total_pages_detected)
print 'missed pages ' + str(No_pages_mismatched)
print 'false hit pages ' + str(not_in_range)
print 'hit pages '  + str(No_pages_matched)
